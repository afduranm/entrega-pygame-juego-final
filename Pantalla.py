from pygame import *
import pygame
import sys 
from random import randint

ancho=800
alto=600
class Pelotas(pygame.sprite.Sprite):
    def __init__(self,posx,posy,ruta,personaje):
        pygame.sprite.Sprite.__init__(self)
        
        self.imagePelota = pygame.image.load('pelota.png')
        self.rect = self.imagePelota.get_rect()
        self.velocidadPelota = 5
        
        self.rect.top=posy
        self.rect.left=posx
        
        self.disparoPersonaje = personaje
        
    
    def trayectoria(self):
        if self.disparoPersonaje == True:
            self.rect.left = self.rect.left - self.velocidadPelota
        else:
            self.rect.left = self.rect.left + self.velocidadPelota
            
        
    def dibujar(self,superficie):
        superficie.blit(self.imagePelota, self.rect)


class Carrera:
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.ImagenP1 = pygame.image.load('cicla.png.png')
        self.rect = self.ImagenP1.get_rect()
        self.rect.x = 100
        self.rect.y = 400
        self.clock=pygame.time.Clock()
        self.velocidad=10
        self.Vida = True
        self.isJump = True  
        self.salto = 20  
        self.listadetiros= [] 
        
    def movimientoDerecha(self):
        self.rect.right += self.velocidad
        self._movimiento()
        
    def movimientoIzquierda(self):
        self.rect.right -= self.velocidad
        self._movimiento()
        
    def _movimiento(self):
        if self.Vida == True:
            if self.rect.left <=0:
                self.rect.left = 0
            elif self.rect.right>=800:
                self.rect = 800  
                
    def lanzar (self, x, y):
        miPelota = Pelotas(x,y,"personaje2.png", True)
        self.listadetiros.append(miPelota)
   
    def dibujar(self,superficie):

        superficie.blit(self.ImagenP1,self.rect)
        
class Personajedos (pygame.sprite.Sprite):
    def __init__(self,posx,posy):
        pygame.sprite.Sprite.__init__(self)
        self.personaje2 = pygame.image.load ('personaje2.png')
        self.rect = self.personaje2.get_rect()
        
        self.listadetiros = []
        self.velocidad = 20
        self.rect.top=posy
        self.rect.left=posx
        
        self.rangoDisparo = 5
        self.tiempoCambio = 1
        
    def trayectoria(self,superficie):
        superficie.blit(self.personaje2, self.rect)
        
    def dibujar(self,superficie):
        superficie.blit(self.personaje2, self.rect)
    
    def comportamiento (self, tiempo):
        self.__ataque()
        if self.tiempoCambio == tiempo:
            #self.posImagen +=1
            self.tiempoCambio +=1
            
            
        
    def __ataque(self):
        if(randint(0,100)<self.rangoDisparo):
            self.__tiros()
            
    def __tiros(self):
        x,y= self.rect.center
        miPelota= Pelotas(x,y,'pelota.png', False)
        self.listadetiros.append(miPelota)
        
        
        
        
def Mapa1():
    pygame.init()
    ventana=pygame.display.set_mode((ancho,alto))
    pygame.display.set_caption("Carrera")
    ImagenFondo= pygame.image.load('parque.png').convert()
    



    jugador = Carrera()
    
    Personaje2 = Personajedos(100,100)
    
    #Demopelota = Pelotas(800,400)
    
    enJuego = True
    
    reloj=pygame.time.Clock()
   
    while True:
        reloj.tick(100)
        
        
        #Demopelota.trayectoria()
        for evento in pygame.event.get():
            if evento.type == QUIT:
                pygame.quit()
                sys.exit()
            
                
            if enJuego == True:
                if evento.type == pygame.KEYDOWN:
                    if evento.key == K_LEFT:
                        jugador.movimientoIzquierda()
                    if evento.key == K_RIGHT:
                        jugador.movimientoDerecha()
                    if evento.key == K_UP: 
                        jugador.rect.y -= jugador.salto
                    if evento.key == K_DOWN:
                        jugador.rect.y += jugador.salto
                    elif evento.key == K_SPACE:
                        x,y = jugador.rect.center
                        jugador.lanzar(x,y)
                            
                if evento.type == pygame.K_UP:
                    if evento.key == K_LEFT:
                        jugador.rect.left = 0
                    if evento.key == K_RIGHT:
                        jugador.rect.right = 0                

        ventana.blit(ImagenFondo,(0,0))

        
        #Demopelota.dibujar(ventana)
        Personaje2.dibujar(ventana)
        jugador.dibujar(ventana)
        Personaje2.dibujar(ventana)
        if len(jugador.listadetiros)>0:
            for x in jugador.listadetiros:
                x.dibujar(ventana)
                x.trayectoria()
                if x.rect.left < 5:
                    jugador.listadetiros.remove(x)
                    
        if len(Personaje2.listadetiros)>0:
            for x in Personaje2.listadetiros:
                x.dibujar(ventana)
                x.trayectoria()
                if x.rect.left > 800:
                    Personaje2.listadetiros.remove(x)        
        pygame.display.update
        display.flip()
        
        
        
Mapa1()


            
            